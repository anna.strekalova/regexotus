﻿using System.Text.RegularExpressions;

namespace RegexOtus
{
    /// <summary>
    /// Класс парсинга строки
    /// </summary>
    public class Parser
    {
        /// <summary>
        /// Получить все адреса изображений
        /// </summary>
        /// <param name="pageHtml">Разметка страницы/param>
        /// <returns>Коллекция всех адресов изображений/returns>
        public static MatchCollection GetAllImgUrlFromHtml(string pageHtml)
        {
            string pattern = @"<img.*?src\s*=\s*[""'](.+?)[""']";
            Regex regex = new Regex(pattern);

            var matches = regex.Matches(pageHtml);
            return matches;
        }

        /// <summary>
        /// Получить название изображения
        /// </summary>
        /// <param name="imgUrl">Адрес изображения</param>
        /// <returns>Название изображения/returns>
        public static string GetImgNameFromUrl(string imgUrl)
        {
            string pattern = @"([^\/])+$";
            Regex regex = new Regex(pattern);
            var matches = regex.Matches(imgUrl);
            return matches[0].Value;
        }
    }
}
