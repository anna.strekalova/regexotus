﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text.RegularExpressions;

namespace RegexOtus
{
    /// <summary>
    /// Класс, предоставляющий логику получения контента страницы и его обработку
    /// </summary>
    public class ContentProvider
    {
        /// <summary>
        /// Получить список адресов изображений со страницы изображения
        /// </summary>
        /// <param name="url">Url страницы</param>
        /// <returns>Список адресов изображений/returns>
        public static List<string> GetAllImgHtmlContent(string url)
        {
            List<string> imgs = new List<string>();

            try
            {
                var pageHtml = GetPageHtmlContent(url);
                var imgAddresses = Parser.GetAllImgUrlFromHtml(pageHtml);
                foreach (Match img in imgAddresses)
                {
                    if (img.Groups[1] != null)
                    {
                        imgs.Add(img.Groups[1].Value);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return imgs;
        }

        /// <summary>
        /// Получить контент html страницы
        /// </summary>
        /// <param name="url">Url страницы</param>
        /// <returns>Контент страницы</returns>
        private static string GetPageHtmlContent(string url)
        {
            try
            {
                using (WebClient wc = new WebClient())
                {
                    return wc.DownloadString(url);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Нельзя получить страницу по заданному адресу {url}.");
                throw ex;
            }
        }
    }
}
