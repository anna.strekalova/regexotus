﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;

namespace RegexOtus
{
    /// <summary>
    /// Класс, предоставляющий логику управление файлами
    /// </summary>
    public class FileManager
    {
        /// <summary>
        /// Создать директорию
        /// </summary>
        /// <param name="path">Путь</param>
        public static void CreateDirectory(string path)
        {
             if (!Directory.Exists(path))
             {
                 Directory.CreateDirectory(path);
             }  
        }

        /// <summary>
        /// Получить имя директории, в которой лежит проект
        /// </summary>
        /// <returns>Имя директории, в которой лежит проект/returns>
        public static string GetProjectDirectory()
        {
            var projectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;

            string pattern = @".*?\\" + projectName;
            Regex regex = new Regex(pattern);

            var solutionPath = Directory.GetCurrentDirectory();

            var savePath = regex.Match(solutionPath).Value;
            return savePath;
        }

        /// <summary>
        /// Сохранить изображение в файл
        /// </summary>
        /// <param name="imgAddress">Адрес изображения</param>
        /// <param name="imgName">Название изображения</param>
        /// <param name="dirPath">Путь для сохранения</param>
        public static void SaveImageToFile(string imgAddress, string imgName, string dirPath)
        {
            try
            {
                using (WebClient wc = new WebClient())
                {
                    wc.DownloadFile(new Uri(imgAddress), dirPath + $"\\{imgName}");
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine($"Неудается сохзранить изображение {imgAddress} в заданную папку {dirPath}.");
                throw ex;
            }
        }
    }
}
