﻿using System;

namespace RegexOtus
{
    class Program
    {
        static void Main(string[] args)
        {
            var url = @"https://www.sites.google.com/site/hudoznikiiiskusstvoxlxveka/osnovnye-predstaviteli";
            try
            {
                var imgContent = ContentProvider.GetAllImgHtmlContent(url);
                if (imgContent.Count > 0)
                {

                    var dir = FileManager.GetProjectDirectory();
                    var dirForSave = dir + "\\Images";
                    FileManager.CreateDirectory(dirForSave);
                    foreach (var img in imgContent)
                    {
                        var imageName = Parser.GetImgNameFromUrl(img);
                        FileManager.SaveImageToFile(img, imageName, dirForSave);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Сбой определения и загрузки изображений по адресу {url}. Exception: {ex}.");
            }
        }
    }
}
